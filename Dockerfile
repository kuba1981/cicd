# syntax=docker/dockerfile:1

###
### Build
###
#FROM golang:1.16-alpine AS build
#
#WORKDIR /app
#
#COPY go.mod ./
#COPY go.sum ./
#RUN go mod download
#
#COPY *.go ./
#
#RUN go build -o /echo
#
###
### Deploy
###
#FROM gcr.io/distroless/base-debian10
#
#WORKDIR /app
#
#COPY --from=build /app /echo
#
#EXPOSE 80
#
#USER nonroot:nonroot
#
#ENTRYPOINT ["/echo"]

FROM golang:1.16 AS builder
WORKDIR /go/src/github.com/alexellis/href-counter/
RUN go get -d -v golang.org/x/net/html
COPY go.mod ./
COPY go.sum ./
RUN go mod download
COPY main.go    ./
RUN CGO_ENABLED=0 GOOS=linux go build -a -installsuffix cgo -o main .

FROM alpine:latest
RUN apk --no-cache add ca-certificates
WORKDIR /root/
COPY --from=builder /go/src/github.com/alexellis/href-counter/main ./
CMD ["./main"]