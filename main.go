package main

import (
	"github.com/labstack/echo/v4"
	"log"
	"net/http"
)

func init() {
	// loads values from .env into the system
	//if err := godotenv.Load(); err != nil {
	//	log.Print("No .env file found")
	//}
}

func main() {
	e := echo.New()

	e.GET("/", hello)

	//port, _ := os.LookupEnv("HTTP_PORT")

	if err := e.Start(":80"); err != http.ErrServerClosed {
		log.Fatal(err)
	}
}

func hello(c echo.Context) error {
	log.Print(`Request send`)
	return c.String(http.StatusOK, "It is a echo framework!It is my CI/CD")
}
